
import tensorflow as tf

### Adam Optimizer
def tf_adam_optimizer( tensors , pars ):

    return tf.train.AdamOptimizer( pars['learning_rate'] ).minimize( tensors[0] )

### Gradient Descent Optimizer
def tf_sgd_optimizer( tensors , pars ):

    return tf.train.GradientDescentOptimizer( pars['learning_rate'] ).minimize( tensors[0] )

