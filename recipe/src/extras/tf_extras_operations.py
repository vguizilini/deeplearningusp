
import tensorflow as tf

### Variable Copy
def tf_var_copy( tensors , pars ):

    return tensors[1].assign( tensors[0] )

### Mean SoftMax Cross Entropy Logit
def tf_mean_soft_cross_logit( tensors , pars ):

    return tf.reduce_mean( \
            tf.nn.softmax_cross_entropy_with_logits( tensors[0] , tensors[1] ) )

### Mean Equal Argmax
def tf_mean_equal_argmax( tensors , pars ):

    correct = tf.equal( tf.argmax( tensors[0] , 1 , name = 'ArgMax_1' ) ,
                        tf.argmax( tensors[1] , 1 , name = 'ArgMax_2' ) )

    return tf.reduce_mean( tf.cast( correct , tf.float32 ) )

### Mean Square Diff
def tf_mean_square_diff( tensors , pars ):

    return tf.reduce_mean( tf.square( tensors[0] - tensors[1] ) )

### Sum Mul
def tf_sum_mul( tensors , pars ):

    return tf.reduce_sum( tf.mul( tensors[0] , tensors[1] ) , reduction_indices = 1 )

### Mean Cast
def tf_mean_cast( tensors , pars ):

    return tf.reduce_mean( tf.cast( tensors[0] , tf.float32 ) )


