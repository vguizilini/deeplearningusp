
import tensorflow as tf

from tf_aux.tf_aux_reshape import *

### 2D Max Pooling
def tf_maxpool2d( x , pars ):

    if not pars['max_pooling_ksize'  ]: pars['max_pooling_ksize']   = pars['max_pooling']
    if not pars['max_pooling_strides']: pars['max_pooling_strides'] = pars['max_pooling']
    if not pars['max_pooling_padding']: pars['max_pooling_padding'] = pars['padding'    ]

    ksize   = spread( pars['max_pooling_ksize'  ] , 2 )
    strides = spread( pars['max_pooling_strides'] , 2 )

    return tf.nn.max_pool( x , ksize   = [ 1 , ksize[0]   , ksize[1]   , 1 ] ,
                               strides = [ 1 , strides[0] , strides[1] , 1 ] ,
                               padding = pars['max_pooling_padding'] )

### 2D Max Pooling
def tf_maxpool3d( x , pars ):

    if not pars['max_pooling_ksize'  ]: pars['max_pooling_ksize']   = pars['max_pooling']
    if not pars['max_pooling_strides']: pars['max_pooling_strides'] = pars['max_pooling']
    if not pars['max_pooling_padding']: pars['max_pooling_padding'] = pars['padding'    ]

    ksize   = spread( pars['max_pooling_ksize'  ] , 3 )
    strides = spread( pars['max_pooling_strides'] , 3 )

    return tf.nn.max_pool3d( x , ksize   = [ 1 , ksize[0]   , ksize[1]   , ksize[2]   , 1 ] ,
                                 strides = [ 1 , strides[0] , strides[1] , strides[2] , 1 ] ,
                                 padding = pars['max_pooling_padding'] )

### Dropout
def tf_dropout( x , dropout ):

    return tf.nn.dropout( x , dropout )
