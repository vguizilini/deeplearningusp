
import tensorflow as tf

from aux_tf_checks import *
from aux_tf_reshape import *
from aux_tf_variables import *

### 2D Max Pooling
def tf_maxpool2d( x , pars ):

    return tf.nn.max_pool( x , strides = [ 1 , pars['max_pooling'] , pars['max_pooling'] , 1 ] ,
                               ksize   = [ 1 , pars['max_pooling'] , pars['max_pooling'] , 1 ] ,
                               padding = pars['padding'] )

### Dropout
def tf_dropout( x , dropout ):

    return tf.nn.dropout( x , dropout )
