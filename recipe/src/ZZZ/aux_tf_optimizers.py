
import tensorflow as tf
from aux_tf_reshape import *

### Adam Optimizer
def tf_adam_optimizer( pars ):

    return tf.train.AdamOptimizer( pars['learning_rate'] , name = 'Adam_Optimizer' )

### Adam Optimizer
def tf_sgd_optimizer( pars ):

    return tf.train.GradientDescentOptimizer( pars['learning_rate'] , name = "SGD_Optimizer" )

