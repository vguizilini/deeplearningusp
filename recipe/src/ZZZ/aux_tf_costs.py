
import tensorflow as tf
from aux_tf_reshape import *

### Mean SoftMax Cross Entropy Logit
def tf_mean_soft_cross_logit( tensors , pars ):

    return tf.reduce_mean( \
            tf.nn.softmax_cross_entropy_with_logits( tensors[0] , tensors[1] ) )

### Mean Equal Argmax
def tf_mean_equal_argmax( tensors , pars ):

    correct = tf.equal( tf.argmax( tensors[0] , 1 , name = 'ArgMax_1' ) ,
                        tf.argmax( tensors[1] , 1 , name = 'ArgMax_2' ) )

    return tf.reduce_mean( tf.cast( correct , tf.float32 ) )
