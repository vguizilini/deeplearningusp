
import numpy as np

### Get Batch Data
def get_batch( data , b , i ):

    l = len( data )

    st = ( i     ) * b % l
    fn = ( i + 1 ) * b % l

    if st > fn :
        return np.vstack( ( data[ st : l ] , data[ 0 : fn ] ) )
    else:
        return data[ st : fn ]

