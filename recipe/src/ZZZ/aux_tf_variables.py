
import tensorflow as tf

### Placeholder
def tf_placeholder( shape = None , name = None ):

    if not shape:
        return tf.placeholder( tf.float32 , name = name )
    return tf.placeholder( tf.float32 , shape , name = name )

### None
def tf_none( shape , pars , name = None ):

    return None

### Copy
def tf_copy( tensor , pars , name = None ):

    return tf.Variable( tensor.initialized_value() , trainable = pars['trainable'] )

### Zeros
def tf_dummy( shape , name = None ):

    return tf.Variable( tf.zeros( name = name ,
                shape = shape , dtype = tf.float32 ) , trainable = False )

### Constant
def tf_constant( shape , pars , name = None ):

    return tf.Variable( tf.constant( pars['value'] , name = name ,
                shape = shape , dtype = tf.float32 ) , trainable = pars['trainable'] )

### Random Normal
def tf_random_normal( shape , pars , name = None ):

    return tf.Variable( tf.random_normal( shape , name = name ,
                mean = pars['mean'] , stddev = pars['stddev'] , seed = pars['seed'] ) , trainable = pars['trainable'] )

### Truncated Normal
def tf_truncated_normal( shape , pars , name = None ):

    return tf.Variable( tf.truncated_normal( shape , name = name ,
                mean = pars['mean'] , stddev = pars['stddev'] , seed = pars['seed'] ) , trainable = pars['trainable'] )

