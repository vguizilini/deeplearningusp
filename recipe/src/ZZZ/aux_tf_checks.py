
import tensorflow as tf

### Check Bias
def checkBias( x , b ):

    if b != None:
        x = tf.nn.bias_add( x , b )
    return x

### Check Sharing
def checkSharing( name ):

    return isinstance( name , str ) or isinstance( name , int )
