
import os
import numpy as np
import tensorflow as tf

import matplotlib.pyplot as plt
from matplotlib.pyplot import clf , plot , draw , show

from aux_reshape import *

### Plot Initialize
def tf_plot_initialize():

    plt.figure()

### Plot Reconstruction
def tf_plot_reconst( x1 , x2 ,
                     epoch = 0 , path = 'figures' ,
                     shape = None ):

    if len( x1.shape ) == 2:
        s = side2D( x1.shape[1] ) ; x1 = x1.reshape( [ -1 , s , s , 1 ] )
    if len( x2.shape ) == 2:
        s = side2D( x2.shape[1] ) ; x2 = x2.reshape( [ -1 , s , s , 1 ] )

    r = 2 ; c = 5

    k = 0
    for j in range( r ):
        for i in range( c ):

            plt.subplot( 2 * r , c , i + 2 * j * c + 1 )
            plt.imshow( x1[ k , : , : , 0 ] , vmin = 0 , vmax = 1 )
            plt.axis( 'off' )

            plt.subplot( 2 * r , c , i + 2 * j * c + c + 1 )
            plt.imshow( x2[ k , : , : , 0 ] , vmin = 0 , vmax = 1 )
            plt.axis( 'off' )

            k = k + 1

    if not os.path.exists( path ): os.makedirs( path )
    plt.savefig( path + '/reconst%d.png' % epoch , bbox_inches = 'tight' )
