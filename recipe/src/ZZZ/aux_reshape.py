
import math

### Flat Shape
def flat_shape( shape ):

    if len( shape ) == 2: return shape[1]
    return shape[1] * shape[2] * shape[3]

### Flat Dimension
def flat_dim( x ):

    return flat_shape( x.shape )

### Flatten
def flatten( x ):

    return x.reshape( [ -1 , flat_dim( x ) ] )

### 2D Side Dimension
def side2D( x , d = 1 ):

    if isinstance( x , list ) : x = x[1]
    return int( round( math.pow( x / d , 1.0 / 2.0 ) ) )

### 3D Side Dimension
def side3D( x , d = 1 ):

    if isinstance( x , list ) : x = x[1]
    return int( round( math.pow( x[1] / d , 1.0 / 3.0 ) ) )

### Shape 1D
def shape1D( x ):

    return [ None , x.shape[1] ]

### Shape 2D
def shape2D( x , channels = 1 ):

    side = side2D( x.shape[1] , channels )
    return [ None , side , side , channels ]

### Shape 3D
def shape3D( x , channels = 1 ):

    side = side3D( x.shape[1] , channels )
    return [ None , side , side , side , channels ]
