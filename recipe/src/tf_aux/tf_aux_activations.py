
import tensorflow as tf

### ReLU Activation
def tf_relu( x ):

    return tf.nn.relu( x )

### Tanh Activation
def tf_tanh( x ):

    return tf.nn.tanh( x )

### SoftPlus Activation
def tf_softplus( x ):

    return tf.nn.softplus( x )

### SoftMax Activation
def tf_softmax( x ):

    return tf.nn.softmax( x )

### Sigmoid Activation
def tf_sigmoid( x ):

    return tf.nn.sigmoid( x )
