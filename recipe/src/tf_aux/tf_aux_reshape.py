
import tensorflow as tf
from aux.aux_reshape import *

### Get Shape
def tf_shape( x ):

    return x.get_shape().as_list()

### Flat Dimension
def tf_flat_dim( x ):

    return flat_dim( tf_shape( x ) )

### Flatten
def tf_flatten( x ):

    return tf.reshape( x , [ -1 , tf_flat_dim( x ) ] )

### 2D Side Dimension
def tf_side2D( x , d = 1 ):

    return side2D( tf_shape( x ) , d )

### 3D Side Dimension
def tf_side3D( x , d = 1 ):

    return side3D( tf_shape( x ) , d )

### 2D Fold
def tf_fold2D( x , d = 1 ):

    s = tf_side2D( x , d )
    return tf.reshape( x , shape = [ -1 , s , s , d ] )

### 3D Fold
def tf_fold3D( x , d = 1 ):

    s = tf_side3D( x , d )
    return tf.reshape( x , shape = [ -1 , s , s , s , d ] )
