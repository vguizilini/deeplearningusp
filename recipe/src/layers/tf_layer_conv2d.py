
import numpy as np
import tensorflow as tf

from tf_aux.tf_aux_checks import *
from tf_aux.tf_aux_reshape import *

class tf_layer_conv2d:

####### Data

    def name(): return 'Conv2D'
    def shapeMult(): return 1
    def dims(): return 2

####### Function
    def function( x , W , b , pars ):

        if len( tf_shape( x ) ) == 2:
            x = tf_fold2D( x , tf_shape( W )[-2] )

        strides = spread( pars['strides'] , 2 )

        layer = tf.nn.conv2d( x , W , name = 'Conv2D' ,
                                      strides = [ 1 , strides[0] , strides[1] , 1 ] ,
                                      padding = pars['padding'] )
        layer = checkBias( layer , b )

        return [ layer ] , None

####### Shapes
    def shapes( input_shape , pars ):

        in_channels = pars['in_channels']
        if len( input_shape ) > 2: in_channels = input_shape[-1]
        out_channels =  pars['out_channels']

        ksize = spread( pars['ksize'] , 2 )

        weight_shape = [ ksize[0] , ksize[1] , in_channels , out_channels ]
        bias_shape   = [                                     out_channels ]

        return weight_shape , bias_shape
