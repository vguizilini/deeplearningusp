
import numpy as np
import tensorflow as tf

from tf_aux.tf_aux_checks import *
from tf_aux.tf_aux_reshape import *

class tf_layer_lstm:

####### Data

    def name(): return 'LSTM'
    def shapeMult(): return 1
    def dims(): return 1

####### Function
    def function( x , W , b , pars ):

        if len( tf_shape( x ) ) == 2:
            x = tf_fold2D( x , 1 )

        layer = tf.transpose( x , [ 1 , 0 , 2 , 3 ] )
        layer = tf.reshape( layer , [ -1 , tf_shape( x )[2] ] )
        layer = tf.split( 0 , tf_shape( x )[1] , layer )

        lstm_cell = tf.nn.rnn_cell.BasicLSTMCell( pars['out_channels'] , forget_bias = 1.0 , state_is_tuple = True )
        outputs , states = tf.nn.rnn( lstm_cell , layer , dtype = tf.float32 )

        WW = tf.get_default_graph().get_tensor_by_name( pars['group_name'] + pars['layer_name'] + '/RNN/BasicLSTMCell/Linear/Matrix:0' )
        bb = tf.get_default_graph().get_tensor_by_name( pars['group_name'] + pars['layer_name'] + '/RNN/BasicLSTMCell/Linear/Bias:0' )

        return [ outputs[-1] , outputs , states ] , [ WW , bb ]

####### Shapes
    def shapes( input_shape , pars ):

        return None , None
