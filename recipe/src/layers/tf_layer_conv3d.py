
import numpy as np
import tensorflow as tf

from tf_aux.tf_aux_checks import *
from tf_aux.tf_aux_reshape import *

class tf_layer_conv3d:

####### Data

    def name(): return 'Conv3D'
    def shapeMult(): return 1
    def dims(): return 3

####### Function
    def function( x , W , b , pars ):

        if len( tf_shape( x ) ) == 2:
            x = tf_fold3D( x , tf_shape( W )[-2] )

        strides = spread( pars['strides'] , 3 )

        layer = tf.nn.conv3d( x , W , name = 'Conv3D' ,
                                      strides = [ 1 , strides[0] , strides[1] , strides[2] , 1 ] ,
                                      padding = pars['padding'] )
        layer = checkBias( layer , b )

        return [ layer ] , None

####### Shapes
    def shapes( input_shape , pars ):

        in_channels = pars['in_channels']
        if len( input_shape ) > 2: in_channels = input_shape[-1]
        out_channels =  pars['out_channels']

        ksize = spread( pars['ksize'] , 3 )

        weight_shape = [ ksize[0] , ksize[1] , ksize[2] , in_channels , out_channels ]
        bias_shape   = [                                                out_channels ]

        return weight_shape , bias_shape
