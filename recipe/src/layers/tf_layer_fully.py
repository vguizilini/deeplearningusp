
import numpy as np
import tensorflow as tf

from tf_aux.tf_aux_checks import *
from tf_aux.tf_aux_reshape import *

class tf_layer_fully:

####### Data

    def name(): return 'Fully'
    def shapeMult(): return 1
    def dims(): return 1

####### Function
    def function( x , W , b , pars ):

        if len( tf_shape( x ) ) > 2:
            x = tf_flatten( x )

        layer = tf.matmul( x , W , name = 'MatMul' )
        layer = checkBias( layer , b )

        return [ layer ] , None

####### Shapes
    def shapes( input_shape , pars ):

        in_channels = flat_dim( input_shape )
        out_channels =  pars['out_channels'] * np.prod( pars['out_side'] )

        weight_shape = [ in_channels , out_channels ]
        bias_shape   = [               out_channels ]

        return weight_shape , bias_shape
