
import numpy as np
import tensorflow as tf

from aux_tf_checks import *
from aux_tf_reshape import *
from aux_tf_variables import *

class tf_layer_deconv2d:

    ### Name
    def name():

        return 'Deconv2D'

    ### Function
    def function( x , W , b , pars ):

        if len( tf_shape( x ) ) == 2:
            x = tf_fold2D( x , tf_shape( W )[-1] )

        with tf.name_scope( 'Deconv2D' ):

            size_batch = tf.shape( x , name = 'batch' )[0]
            out_channels = tf_shape( W )[-2]

            out_shape = tf.pack( [ size_batch , pars['out_side'][0] ,
                                                pars['out_side'][1] , out_channels ] , name = 'shape' )

            layer = tf.nn.conv2d_transpose( x , W , name = 'Deconv2D' ,
                                                    output_shape = out_shape ,
                                                    strides = [ 1 , pars['strides'] , pars['strides'] , 1 ] ,
                                                    padding = pars['padding'] )

            dummy = tf_dummy( [ pars['out_side'][0] ,
                                pars['out_side'][1] , out_channels ] , name = 'dummy' )
            layer = tf.add( layer , dummy , name = 'DummyAdd' )

        layer = checkBias( layer , b )

        return [ layer ]

    ### Shape
    def variableShapes( pars ):

        weight_shape = [ pars['ksize'] , pars['ksize'] , pars['out_channels'] , pars['in_channels'] ]
        bias_shape = [ pars['out_channels'] ]

        return weight_shape , bias_shape

    ### Shape Mult
    def variableShapesMult():

        return 1

    ### Input Channels
    def inputChannels( layers , shape , out_side , pars ):

        if not pars['in_channels']:
            if len( shape ) == 2:
                return int( shape[-1] / np.prod( out_side ) )
            return shape[-1]
        return pars['in_channels']

    ### Output Side
    def outputSide( layers , shape , out_side , pars ):

        if not pars['out_side']:
            return [ int( np.floor( v * pars['strides'] ) ) for v in out_side ]
        return [ pars['out_side'] , pars['out_side'] ]

    ### Set Strides
    def setStrides( layer , shape , out_side , pars ):

        return int( np.ceil( pars['out_side'][0] / out_side[0] ) )



