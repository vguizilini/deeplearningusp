
import numpy as np
import tensorflow as tf

from aux_tf_checks import *
from aux_tf_reshape import *
from aux_tf_variables import *

class tf_layer_variational:

    ### Name
    def name():

        return 'Variational'

    ### Function
    def function( x , W , b , pars ):

        if len( tf_shape( x ) ) > 2:
            x = tf_flatten( x )

        in_channels = tf.shape( x )[0]
        out_channels = int( tf_shape( W )[1] / 2 )

        layer = tf.matmul( x , W , name = 'MatMul' )
        layer = checkBias( layer , b )

        z_mu = layer[ : , :out_channels ]
        z_sig = layer[ : , out_channels: ]

        epsilon = tf.random_normal( tf.pack( [ in_channels , out_channels ] ) , mean = 0.0 , stddev = 1.0 )
        layer = z_mu + z_sig * epsilon

        return [ layer , z_mu , z_sig ]

    ### Shape
    def variableShapes( pars ):

        weight_shape = [ pars['in_channels'] , pars['out_channels'] * np.prod( pars['out_side'] ) ]
        bias_shape = [ pars['out_channels'] * np.prod( pars['out_side'] ) ]

        return weight_shape , bias_shape

    ### Shape Mult
    def variableShapesMult():

        return 2

    ### Input Channels
    def inputChannels( layers , shape , out_side , pars ):

        return tf_flat_dim( layers[-1] )

    ### Output Side
    def outputSide( layers , shape , out_side , pars ):

        return 1

    ### Set Strides
    def setStrides( layer , shape , out_side , pars ):

        return pars['strides']



