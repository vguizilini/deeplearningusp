
import sys
sys.path.append( '..' )
from Recipe import *

# Load Dataset

train_images = np.load( '../data/mnist_train_images.npy' )
train_labels = np.load( '../data/mnist_train_labels.npy' )
test_images  = np.load( '../data/mnist_test_images.npy'  )
test_labels  = np.load( '../data/mnist_test_labels.npy'  )

# Start Recipe

recipe = Recipe()

# Add Input and Label Placeholders

recipe.addInput( train_labels.shape , name = 'Label' )
recipe.addInput( train_images.shape , name = 'Input' )

# Number of Channels

out_channels = [ 3 , 4 ]

# Add Fully Connected Layers

group1 = recipe.addGroup( '/Group1' )

group1.setLayerDefaults( layer_type = tf_layer_fully ,
                         activation = tf_relu )

group1.addLayer( out_channels = out_channels[0] , layer_name = 'Hidden1' , input = '/Input' )
group1.addLayer( out_channels = out_channels[1] , layer_name = 'Hidden2' )

# Add Output Layer

group1.addLayer( out_channels = '/Label' , activation = None , layer_name = "Output" )

# Add Operations

recipe.addOperation( function = tf_mean_soft_cross_logit ,
                     input = [ '/Group1/Output' , '/Label' ] , name = 'Cost1' )

recipe.addOperation( function = tf_mean_equal_argmax ,
                     input = [ '/Group1/Output' , '/Label' ] , name = 'Evaluation' )

recipe.addOperation( function = tf_adam_optimizer ,
                     input = 'Cost1' , name = 'Optimizer1' )

# Initialize Variables

recipe.repeat( '/Group1' , '/Group2'  )
recipe.share( '/Group1' , '/Group3'  )

# Add Operations

recipe.addOperation( function = tf_mean_soft_cross_logit ,
                     input = [ '/Group2/Output' , '/Label' ] , name = 'Cost2' )

recipe.addOperation( function = tf_adam_optimizer ,
                     input = 'Cost2' , name = 'Optimizer2' )

recipe.addOperation( function = tf_mean_soft_cross_logit ,
                     input = [ '/Group3/Output' , '/Label' ] , name = 'Cost3' )

recipe.addOperation( function = tf_adam_optimizer ,
                     input = 'Cost3' , name = 'Optimizer3' )

recipe.addOperation( function = tf_var_copy ,
                     input = [ '/Group1/W_Hidden2' , '/Group2/W_Hidden2' ] , name = 'Copy' )

# Initialize

recipe.initialize()
recipe.printVariables()

# Train

print( recipe.eval( '/Group1/W_Hidden2' ) )
print( recipe.eval( '/Group2/W_Hidden2' ) )
print( recipe.eval( '/Group3/W_Hidden2' ) )

recipe.train( train_data = train_images , train_labels = train_labels ,
              test_data = test_images[ :1000 , : ] , test_labels = test_labels[ :1000 , : ] ,
              optimizer = 'Optimizer1' , eval_function = [ 'Cost1' , 'Evaluation' ] , freq_eval = 1 ,
              size_batch = 1000 , num_epochs = 5 )

print( recipe.eval( '/Group1/W_Hidden2' ) )
print( recipe.eval( '/Group2/W_Hidden2' ) )
print( recipe.eval( '/Group3/W_Hidden2' ) )

recipe.train( train_data = train_images , train_labels = train_labels ,
              test_data = test_images[ :1000 , : ] , test_labels = test_labels[ :1000 , : ] ,
              optimizer = 'Optimizer2' , eval_function = [ 'Cost2' , 'Evaluation' ] , freq_eval = 1 ,
              size_batch = 1000 , num_epochs = 10 )

print( recipe.eval( '/Group1/W_Hidden2' ) )
print( recipe.eval( '/Group2/W_Hidden2' ) )
print( recipe.eval( '/Group3/W_Hidden2' ) )

recipe.eval( 'Copy' )

print( '$$$$$$$$$$$$$$$$$$$$$$$$$$' )

print( recipe.eval( '/Group1/W_Hidden2' ) )
print( recipe.eval( '/Group2/W_Hidden2' ) )
print( recipe.eval( '/Group3/W_Hidden2' ) )
