
import sys
sys.path.append( '..' )
from Recipe import *

### Load Dataset

train_images = np.load( '../../data/mnist_train_images.npy' )
train_labels = np.load( '../../data/mnist_train_labels.npy' )
test_images  = np.load( '../../data/mnist_test_images.npy'  )
test_labels  = np.load( '../../data/mnist_test_labels.npy'  )

### Create Recipe

channels = [ 32 , 64 , 128 ]
latent_size = 50

recipe = Recipe( shape1D( train_images ) ,
                 shape1D( train_images ) )

recipe.setDefaults( layer_type = 'Fully' )

recipe.addNewLayer( layer_name = 'fully_1' , out_channels = channels[0] )
recipe.addNewLayer( layer_name = 'fully_2' , out_channels = channels[1] )
recipe.addNewLayer( layer_name = 'fully_3' , out_channels = channels[2] )

recipe.addNewLayer( layer_name = 'encode'  , out_channels = latent_size , layer_type = 'Variational' , activation = None )
recipe.addNewLayer( layer_name = 'decode'  , out_channels = channels[2] )

recipe.addNewLayer( layer_name = 'fully_4' , out_channels = channels[1] )
recipe.addNewLayer( layer_name = 'fully_5' , out_channels = channels[0] )
recipe.addNewLayer( layer_name = 'fully_6' , out_channels = 784 , activation = tf_sigmoid )

recipe.setCostFunction( tf_mean_variational , encoded_layer = 'encode' )
recipe.setOptimizer( tf_adam_optimizer , learning_rate = 1e-3 )
recipe.setEvalFunction( tf_mean_squared_error )
recipe.setPlotFunction( tf_plot_reconst )

### Run

recipe.initialize()
recipe.train( train_data = train_images , train_labels = train_images ,
              size_batch = 100 , num_epochs = 100 ,
              eval_data = test_images  , eval_labels = test_images  ,
              size_eval = 10 , freq_epoch_eval = 1 , path_plot = 'figures/vae' )

