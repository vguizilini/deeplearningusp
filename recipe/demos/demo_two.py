
import sys
sys.path.append( '..' )
from Recipe import *

### Load Dataset

train_images = np.load( '../../data/mnist_train_images.npy' )
train_labels = np.load( '../../data/mnist_train_labels.npy' )
test_images  = np.load( '../../data/mnist_test_images.npy'  )
test_labels  = np.load( '../../data/mnist_test_labels.npy'  )

### Create Recipe 1

channels = [ 100 ]

recipe1 = Recipe( shape1D( train_images ) ,
                  shape1D( train_labels ) , name = 'Recipe1' )

recipe1.setDefaults( layer_type = tf_layer_fully )

recipe1.addNewLayer( out_channels = channels[0] )
recipe1.addNewLayer( out_channels = 'output' , activation = tf_softmax )

recipe1.setCostFunction( tf_mean_soft_cross_logit )
recipe1.setOptimizer( tf_adam_optimizer , learning_rate = 1e-3 )
recipe1.setEvalFunction( tf_mean_equal_argmax )
recipe1.initialize()

#### Create Recipe 2

channels = [ 100 , 200 ]

recipe2 = Recipe( shape1D( train_images ) ,
                  shape1D( train_labels ) , name = 'Recipe2' )

recipe2.setDefaults( layer_type = tf_layer_fully )

recipe2.addNewLayer( out_channels = channels[0] )
recipe2.addNewLayer( out_channels = channels[1] )
recipe2.addNewLayer( out_channels = 'output' , activation = tf_softmax )

recipe2.setCostFunction( tf_mean_soft_cross_logit )
recipe2.setOptimizer( tf_adam_optimizer , learning_rate = 1e-3 )
recipe2.setEvalFunction( tf_mean_equal_argmax )
recipe2.initialize()

# Run

recipe1.train( train_data = train_images , train_labels = train_labels ,
               size_batch = 500 , num_epochs = 5 ,
               eval_data = test_images  , eval_labels = test_labels  ,
               freq_epoch_eval = 1 )

recipe2.train( train_data = train_images , train_labels = train_labels ,
               size_batch = 500 , num_epochs = 5 ,
               eval_data = test_images  , eval_labels = test_labels  ,
               freq_epoch_eval = 1 )

