
import sys
sys.path.append( '..' )
from Recipe import *

# Load Dataset

train_data   = np.load( '../data/model3d_train_grid.npy'   )
train_labels = np.load( '../data/model3d_train_labels.npy' )
test_data    = np.load( '../data/model3d_test_grid.npy'    )
test_labels  = np.load( '../data/model3d_test_labels.npy'  )

## Start Recipe

recipe = Recipe()

## Add Input and Label Placeholders

recipe.addInput( train_labels.shape , name = 'Label' )
recipe.addInput( train_data.shape   , name = 'Input' )

## Number of Channels

out_conv_channels = [ 64 , 128 ]
out_fully_channels  = [ 1024 ]

## Add 3D Convolutional Layers

recipe.setLayerDefaults( layer_type = tf_layer_conv3d ,
                         ksize = 5 , max_pooling = 2 , activation = tf_relu )

for n in out_conv_channels:
    recipe.addLayer( out_channels = n )

## Add Fully Connected Layers

recipe.setLayerDefaults( layer_type = tf_layer_fully ,
                         activation = tf_relu )

for n in out_fully_channels:
    recipe.addLayer( out_channels = n , dropout = 0.5 )

## Add Output Layer

recipe.addLayer( out_channels = 'Label' , activation = None , layer_name = "Output" )

## Add Cost and Evaluation Operations

recipe.addOperation( tf_mean_soft_cross_logit , [ 'Output' , 'Label' ] , name = 'Cost'       )
recipe.addOperation( tf_mean_equal_argmax ,     [ 'Output' , 'Label' ] , name = 'Evaluation' )

## Add Optimizer Operation

recipe.addOperation( tf_adam_optimizer , 'Cost' , name = 'Optimizer' )

## Initialize Variables

recipe.initialize()
recipe.printNodes()

## Train

recipe.train( train_data = train_data , train_labels = train_labels ,
              test_data = test_data[ :1000 , : ] , test_labels = test_labels[ :1000 , : ] ,
              optimizer = 'Optimizer' , eval_function = [ 'Cost' , 'Evaluation' ] , freq_eval = 1 ,
              size_batch = 1000 , num_epochs = 500 )
