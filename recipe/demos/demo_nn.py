
import sys
sys.path.append( '..' )
from Recipe import *

# Load Dataset

train_images = np.load( '../../data/mnist_train_images.npy' )
train_labels = np.load( '../../data/mnist_train_labels.npy' )
test_images  = np.load( '../../data/mnist_test_images.npy'  )
test_labels  = np.load( '../../data/mnist_test_labels.npy'  )

# Start Recipe

recipe = Recipe()

# Add Input and Label Placeholders

recipe.addInput( train_labels.shape , name = 'Label' )
recipe.addInput( train_images.shape , name = 'Input' )

# Number of Channels

out_channels = [ 200 , 200 ]

# Add Fully Connected Layers

recipe.setLayerDefaults( layer_type = tf_layer_fully ,
                         activation = tf_relu )

for n in out_channels:
    recipe.addLayer( out_channels = n )

# Add Output Layer

recipe.addLayer( out_channels = 'Label' , activation = None , layer_name = "Output" )

# Add Cost and Evaluation Operations

recipe.addOperation( tf_mean_soft_cross_logit , [ 'Output' , 'Label' ] , name = 'Cost'       )
recipe.addOperation( tf_mean_equal_argmax ,     [ 'Output' , 'Label' ] , name = 'Evaluation' )

# Add Optimizer Operation

recipe.addOperation( tf_adam_optimizer , 'Cost' , name = 'Optimizer' )

# Initialize Variables

recipe.initialize()
recipe.printNodes()

# Train

recipe.train( train_data = train_images , train_labels = train_labels ,
              test_data = test_images[ :1000 , : ] , test_labels = test_labels[ :1000 , : ] ,
              optimizer = 'Optimizer' , eval_function = [ 'Cost' , 'Evaluation' ] , freq_eval = 1 ,
              size_batch = 1000 , num_epochs = 500 )
