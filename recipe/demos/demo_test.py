
import sys
sys.path.append( '..' )
from Recipe import *

### Load Dataset

#train_data   = np.load( '../../data/mnist_train_images.npy' )
#train_labels = np.load( '../../data/mnist_train_labels.npy' )
#test_data    = np.load( '../../data/mnist_test_images.npy'  )
#test_labels  = np.load( '../../data/mnist_test_labels.npy'  )

train_data = np.ones( [ 100 , 3 ] )
train_labels = np.zeros( [ 100 , 1 ] )

### Create Recipe

channels = [ 32 , 64 , 128 ]

recipe = Recipe()

recipe.addInput( train_data.shape )
recipe.addLabel( train_labels.shape )

recipe.setLayerDefaults( layer_type = tf_layer_fully )

recipe.addLayer( input = 'Input_0' , out_channels = 2 )
recipe.addLayer( input = 'Input_0' , out_channels = 2 )
recipe.addLayer( input = 'Fully_0' , out_channels = 2 )
recipe.addLayer( input = 'Fully_2' , out_channels = 2 , weight_share = 'W_Fully_2' )
recipe.addLayer( input = 'Fully_3' , out_channels = 2 , weight_copy  = 'W_Fully_2' )
recipe.addLayer( input = 'Fully_4' , out_channels = 2 , weight_share  = 'W_Fully_2' )
recipe.addLayer( input = 'Fully_5' , layer_name = 'Output' , out_channels = 1 )

recipe.addOperation( tf_mean_soft_cross_logit , [ 'Output' , 'Label_0' ] )
recipe.addOptimizer( tf_adam_optimizer , 'Operation_0' )

recipe.initialize()

print('$$$$$')
print( recipe.getEval( 'W_Fully_0' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_1' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_2' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_3' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_4' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_5' ) )
print('$$$$$')

recipe.train( train_data = train_data , train_labels = train_labels ,
              optimizer = 'Optimizer_0' , size_batch = 1 , num_epochs = 10 )


print('$$$$$')
print( recipe.getEval( 'W_Fully_0' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_1' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_2' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_3' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_4' ) )
print('$$$$$')
print( recipe.getEval( 'W_Fully_5' ) )
print('$$$$$')
