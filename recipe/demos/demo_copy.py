

import sys
sys.path.append( '..' )
from Recipe import *

### Load Dataset

#train_data   = np.load( '../../data/mnist_train_images.npy' )
#train_labels = np.load( '../../data/mnist_train_labels.npy' )
#test_data    = np.load( '../../data/mnist_test_images.npy'  )
#test_labels  = np.load( '../../data/mnist_test_labels.npy'  )

train_data = 100 * np.ones( [ 100 , 3 ] )
train_labels = np.zeros( [ 100 , 1 ] )

### Create Recipe

channels = [ 32 , 64 , 128 ]

recipe1 = Recipe( name = 'Recipe1' )

recipe1.addInput( train_data.shape )
recipe1.addLabel( train_labels.shape )

recipe1.setLayerDefaults( layer_type = tf_layer_fully )

recipe1.addLayer( input = 'Input_0' , out_channels = 2 )
recipe1.addLayer( input = 'Input_0' , out_channels = 2 )
recipe1.addLayer( input = 'Fully_0' , out_channels = 2 )
recipe1.addLayer( input = 'Fully_2' , out_channels = 2 , weight_share = 'W_Fully_2' )
recipe1.addLayer( input = 'Fully_3' , out_channels = 2 , weight_copy  = 'W_Fully_2' )
recipe1.addLayer( input = 'Fully_4' , out_channels = 2 , weight_share = 'W_Fully_2' )
recipe1.addLayer( input = 'Fully_5' , layer_name = 'Output' , out_channels = 1 )

recipe1.addOperation( function = tf_mean_soft_cross_logit , input = [ 'Output' , 'Label_0' ] )
recipe1.addOptimizer( function = tf_adam_optimizer , input = 'Operation_0' )

recipe2 = recipe1.copy( 'Recipe2' )

print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ RECIPE 1 BEFORE')
print( recipe1.getEval( 'W_Fully_0' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_1' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_2' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_3' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_4' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_5' ) )
print('$$$$$')

print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ RECIPE 2 BEFORE')
print( recipe2.getEval( 'W_Fully_0' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_1' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_2' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_3' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_4' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_5' ) )
print('$$$$$')

recipe1.train( train_data = train_data , train_labels = train_labels ,
              optimizer = 'Optimizer_0' , size_batch = 1 , num_epochs = 100 )

recipe2.train( train_data = train_data , train_labels = train_labels ,
              optimizer = 'Optimizer_0' , size_batch = 1 , num_epochs = 10 )

print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ RECIPE 1 AFTER')
print( recipe1.getEval( 'W_Fully_0' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_1' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_2' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_3' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_4' ) )
print('$$$$$')
print( recipe1.getEval( 'W_Fully_5' ) )
print('$$$$$')

print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ RECIPE 2 AFTER')
print( recipe2.getEval( 'W_Fully_0' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_1' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_2' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_3' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_4' ) )
print('$$$$$')
print( recipe2.getEval( 'W_Fully_5' ) )
print('$$$$$')

