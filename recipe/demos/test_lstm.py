
import sys
sys.path.append( '..' )
from Recipe import *

recipe = Recipe()

recipe.addInput( [ None , 4 ] , name = 'Input' )

group1 = recipe.addGroup( '/Group1' )

group1.addLayer( layer_type = tf_layer_lstm , input = '/Input' ,
                 out_channels = 5 , layer_name = 'LSTM' )

recipe.repeat( '/Group1' , '/Group2' )

recipe.initialize()

print( '#################' )
print( recipe.eval( '/Group1/W_LSTM' ) )
print( '#################' )
print( recipe.eval( '/Group2/W_LSTM' ) )
print( '#################' )



