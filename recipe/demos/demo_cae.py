
import sys
sys.path.append( '..' )
from Recipe import *

### Load Dataset

train_images = np.load( '../../data/mnist_train_images.npy' )
train_labels = np.load( '../../data/mnist_train_labels.npy' )
test_images  = np.load( '../../data/mnist_test_images.npy'  )
test_labels  = np.load( '../../data/mnist_test_labels.npy'  )

### Create Recipe

channels = [ 32 , 64 , 128 ]
latent_size = 50

recipe = Recipe( shape2D( train_images ) ,
                 shape2D( train_images ) )

recipe.setDefaults( layer_type = tf_layer_conv2d , strides = 2 , ksize = 3 )
recipe.addNewLayer( layer_name = 'conv_1' , out_channels = channels[0] )
recipe.addNewLayer( layer_name = 'conv_2' , out_channels = channels[1] )
recipe.addNewLayer( layer_name = 'conv_3' , out_channels = channels[2] )

recipe.setDefaults( layer_type = tf_layer_fully )
recipe.addNewLayer( layer_name = 'encode' , out_channels = latent_size )
recipe.addNewLayer( layer_name = 'decode' , out_channels = channels[2] , out_side = 'conv_3' )

recipe.setDefaults( layer_type = tf_layer_deconv2d )
recipe.addNewLayer( layer_name = 'deconv_1' , out_channels = channels[1] , out_side = 'conv_2' )
recipe.addNewLayer( layer_name = 'deconv_2' , out_channels = channels[0] , out_side = 'conv_1' )
recipe.addNewLayer( layer_name = 'deconv_3' , out_channels = 'output' )

recipe.setCostFunction( tf_mean_squared_error )
recipe.setOptimizer( tf_adam_optimizer , learning_rate = 1e-3 )
recipe.setEvalFunction( tf_mean_squared_error )
recipe.setPlotFunction( tf_plot_reconst )

### Run

recipe.initialize()
recipe.train( train_data = train_images , train_labels = train_images ,
              size_batch = 100 , num_epochs = 50 ,
              eval_data = test_images  , eval_labels = test_images  ,
              size_eval = 10 , freq_epoch_eval = 1 , path_plot = 'figures/cae' )

