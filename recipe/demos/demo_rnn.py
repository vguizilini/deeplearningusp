
import sys
sys.path.append( '..' )
from Recipe import *

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets( "./data_mnist/" , one_hot = True )

learning_rate  = 0.001
training_iters = 100000
batch_size     = 128
display_step   = 10

n_input   = 28
n_steps   = 28
n_hidden  = 128
n_classes = 10

recipe = Recipe()

recipe.addInput( [ None , n_classes ] , name = 'Label' )
recipe.addInput( [ None , n_steps * n_input ] , name = 'Input' )

recipe.addLayer( layer_type = tf_layer_lstm , out_channels = n_hidden ,
                 input = 'Input' , layer_name = 'LSTM1' )

recipe.addLayer( layer_type = tf_layer_lstm , out_channels = n_hidden ,
                 input = 'Input' , layer_name = 'LSTM2' )

recipe.addLayer( layer_type = tf_layer_fully , out_channels = n_classes ,
                 input = 'LSTM1' , activation = None , layer_name = 'Output' , dropout = 0.8 )

recipe.addOperation( function = tf_mean_soft_cross_logit , input = [ 'Output' , 'Label' ] , name = 'Cost'     )
recipe.addOperation( function = tf_mean_equal_argmax     , input = [ 'Output' , 'Label' ] , name = 'Equal'    )
recipe.addOperation( function = tf_mean_cast             , input = [ 'Equal'            ] , name = 'Accuracy' )

recipe.addOperation( function = tf_adam_optimizer , input = 'Cost' ,
                     learning_rate = learning_rate , name = 'Optimizer' )

recipe.initialize()
recipe.printNodes()
recipe.printVariables()

recipe.train( train_data = mnist.train.images , train_labels = mnist.train.labels ,
              test_data = mnist.test.images[ :1000 , : ] , test_labels = mnist.test.labels[ :1000 , : ] ,
              optimizer = 'Optimizer' , eval_function = [ 'Cost' , 'Accuracy' ] , freq_eval = 1 ,
              size_batch = 1000 , num_epochs = 500 )



#accuracy = tf.reduce_mean( tf.cast( correct_pred , tf.float32 ) )

#init = tf.initialize_all_variables()

#with tf.Session() as sess:

#    sess.run( init )
#    step = 1

#    while step * batch_size < training_iters:

#        batch_x , batch_y = mnist.train.next_batch( batch_size )
#        batch_x = batch_x.reshape( ( batch_size , n_steps , n_input ) )

#        sess.run( optimizer , feed_dict = { x : batch_x ,
#                                            y : batch_y } )

#        if step % display_step == 0:

#            acc = sess.run( accuracy , feed_dict = { x : batch_x , y : batch_y } )
#            loss = sess.run( cost , feed_dict = { x : batch_x , y : batch_y } )

#            print( "Iter " + str( step * batch_size ) + ", Minibatch Loss= " + \
#                   "{:.6f}".format( loss ) + ", Training Accuracy= " + \
#                   "{:.5f}".format( acc ) )

#        step += 1

#    print( "Optimization Finished!" )

#    test_len = 128
#    test_data = mnist.test.images[:test_len].reshape( ( -1 , n_steps , n_input ) )
#    test_label = mnist.test.labels[:test_len]

#    print("Testing Accuracy:", \
#        sess.run( accuracy , feed_dict = { x : test_data , y : test_label } ) )
