
import os
import numpy as np

import sys
path = '/home/vguizilini/Documents/code/python/recipe'
sys.path.append( path + '/src' )

from aux.aux_load import *
from aux.aux_batch import *

from tf_aux.tf_aux_checks import *
from tf_aux.tf_aux_reshape import *
from tf_aux.tf_aux_variables import *
from tf_aux.tf_aux_activations import *

from layers.tf_layer_fully import *
from layers.tf_layer_conv2d import *
from layers.tf_layer_conv3d import *
from layers.tf_layer_lstm import *

from extras.tf_extras_layers import *
from extras.tf_extras_operations import *
from extras.tf_extras_optimizers import *

class Recipe:

########################################################################################################

####### Start Variables
    def startVariables( self , name ):

        self.cnt = 0
        self.curr_input = None

        if name:
            self.name = name + '/'
        else:
            self.name = ''

        self.inputs = []
        self.layers = []
        self.extras = []
        self.operations = []

        self.weights = []
        self.biases = []
        self.dropouts = []

        self.names = {}
        self.items = {}

        self.defs_layer = {
            'input' : None , 'layer_type' : None , 'layer_name' : None ,
            'layer_copy' : None , 'layer_share' : None ,
            'weight_type' : tf_truncated_normal , 'weight_name' : None ,
            'weight_mean' : 0.0  , 'weight_stddev' : 0.1 , 'weight_copy' : None , 'weight_share' : None ,
            'weight_value' : 0.0 , 'weight_min' : 0.0 , 'weight_max' : 1.0 ,
            'weight_trainable' : True , 'weight_seed' : None ,
            'bias_type' : tf_truncated_normal ,  'bias_name' : None ,
            'bias_mean' : 0.0 , 'bias_stddev' : 0.1 , 'bias_copy' : None , 'bias_share' : None ,
            'bias_value' : 0.0 , 'bias_min' : 0.0 , 'bias_max' : 1.0 ,
            'bias_trainable' : True , 'bias_seed' : None ,
            'in_channels' : 1 , 'out_channels' : None , 'out_side' : 1 ,
            'max_pooling' : 1 , 'max_pooling_ksize' : None , 'max_pooling_strides' : None , 'max_pooling_padding' : None ,
            'activation' : tf_relu , 'dropout' : 0.0 , 'strides' : 1 , 'ksize' : 3 , 'padding' : 'SAME' ,
        }

        self.defs_operation = {
            'name' : None ,
            'learning_rate' : 1e-4 ,
        }

####### Set Layer Defaults
    def setLayerDefaults( self , **args ):

        self.defs_layer = { **self.defs_layer , **args }

####### Set Operation Defaults
    def setOperationDefaults( self , **args ):

        self.defs_operation = { **self.defs_operation , **args }

####### Add Group
    def addGroup( self , name ):

        if name[0] == '/':
            name = name[1:]
            scope = self.name + name
        else:
            scope = self.name[0:-1]

        self.items[ name ] = Recipe( sess = self.sess , root = self.root , name = scope )
        return self.items[ name ]

####### Constructor
    def __init__( self , sess = None , root = None , name = None ):

        if not sess: sess = tf.Session()
        self.sess = sess

        if not root: root = self;
        self.root = root

        self.startVariables( name )

####### Add Input
    def addInput( self , shape , name = None ):

        idx = len( self.inputs )
        if not name: name = 'Input_' + str( idx )
        self.names[name] = ( 'input' , idx )

        shape = list( shape )
        if len( shape ) > 1: shape[0] = None
        self.curr_input = name

        with tf.variable_scope( '' ):#self.name + name ):
            self.inputs.append( \
                [ tf_placeholder( shape ) , name ] )

####### Add Operation
    def addOperation( self , **args ):

        pars = { **self.defs_operation , **args }

        idx = len( self.operations )
        if not pars['name']: pars['name'] = 'Operation_' + str( idx )
        self.names[ pars['name'] ] = ( 'operation' , idx )

        tensors = self.tensor_list( pars['input'] )

        with tf.variable_scope( '' ):#self.name + pars['name'] ):
            self.operations.append( [ pars['function']( tensors , pars ) , pars ] )

#        tf.scalar_summary( pars['name'] , self.tensor( pars['name'] ) )

####### Add Layer
    def addLayer( self , **args ):

        pars = { **self.defs_layer , **args }
        if not pars['input']: pars['input'] = self.curr_input
        pars = self.createLayerNames( pars )

        if isinstance( pars['out_channels'] , str ):
            pars['out_channels'] = self.shape( pars['out_channels'] )[-1]

        layer = pars['layer_type']
        input_shape = self.shape( pars['input'] )
        pars['group_name'] = self.name

        weight_shape , bias_shape = layer.shapes( input_shape , pars )

        self.newWeight( weight_shape , pars )
        self.newBias( bias_shape , pars )
        self.newLayer( pars )

        self.checkActivation( pars )
        self.checkMaxPooling( pars )
        self.checkDropout(    pars )

        self.curr_input = pars['layer_name']

####### Append Prefix
    def appendPrefix( self , name , pref ):

        res = '' ; list = name.split( '/' )
        for i in range( len( list ) - 1 ):
            res = res + list[i] + '/'
        return res + pref + list[-1]

####### New Variable
    def newVariable( self , shape , pars , type ):

        if type == 'weight': s , p , list = 'weight_' , 'W_' , self.weights
        if type == 'bias':   s , p , list = 'bias_'   , 'b_' , self.biases

        if shape is None:

            list.append( [ None , pars ] )

        else:

            if pars['layer_copy' ]: pars[s + 'copy' ] = self.appendPrefix( pars['layer_copy' ] , p )
            if pars['layer_share']: pars[s + 'share'] = self.appendPrefix( pars['layer_share'] , p )

            if pars[s + 'share']:

                with tf.variable_scope( '' ):#self.name + pars[s + 'scope'] ):
                    list.append( [ pars[s + 'share'] , pars ] )

            else:

                if pars[s + 'copy'] is not None:
                    pars[s + 'type'] = tf_copy
                    if isinstance( pars[s + 'copy'] , str ):
                        shape = self.tensor( pars[s + 'copy'] )
                    else:
                        shape = pars[s + 'copy']
                else:
                    shape[-1] = shape[-1] * pars['layer_type'].shapeMult()

                dict = { 'mean' : pars[s + 'mean'] , 'stddev' : pars[s + 'stddev'] ,
                         'value' : pars[s + 'value'] , 'min' : pars[s + 'min'] , 'max' : pars[s + 'max'] ,
                         'trainable' : pars[s + 'trainable'] , 'seed' : pars[s + 'seed'] }

                with tf.variable_scope( '' ):#self.name + pars[s + 'scope'] ):
                    list.append( [ pars[s + 'type']( shape , dict ) , pars ] )

####### New Weight
    def newWeight( self , shape , pars ):

        self.newVariable( shape , pars , 'weight' )

####### New Bias
    def newBias( self , shape , pars ):

        self.newVariable( shape , pars , 'bias' )

####### New Layer
    def newLayer( self , pars ):

        with tf.variable_scope( '' ):#self.name + pars['layer_name'] ):

            new_layer , new_vars = pars['layer_type'].function(
                                                self.tensor( pars['input'      ] ) , \
                                                self.tensor( pars['weight_name'] ) , \
                                                self.tensor( pars['bias_name'  ] ) , pars )

            if new_vars is not None:

                self.weights[-1][0] = new_vars[0]
                self.biases[ -1][0] = new_vars[1]

        self.layers.append( [ new_layer[0 ] , pars ] )
        self.extras.append(   new_layer[1:] )

        self.cnt = self.cnt + 1

####### Iterate Groups
    def iterateGroups( self , list ):

        ptr = self.root
        for i in range( len( list ) - 1 ):
            if list[i] is not '' : ptr = ptr[ list[i] ]
        return ptr

####### Get Tensor
    def tensor( self , name ):

        list = name.split( '/' )

        if len( list ) == 1 :

            tag , idx = self.names[ name ]

            if tag == 'input':     return self.inputs[     idx ][0]
            if tag == 'layer':     return self.layers[     idx ][0]
            if tag == 'weight':    return self.weights[    idx ][0]
            if tag == 'bias':      return self.biases[     idx ][0]
            if tag == 'operation': return self.operations[ idx ][0]

            return None

        else:

            ptr = self.iterateGroups( list )
            return ptr.tensor( list[-1] )

####### Get Tensor List
    def tensor_list( self , names ):

        if not isinstance( names , list ):
            return [ self.tensor( names ) ]

        tensors = []
        for name in names:
            tensors.append( self.tensor( name ) )
        return tensors

####### Get Recipe
    def __getitem__( self , name ):

        list = name.split( '/' )

        if len( list ) == 1 : return self.items[ name ]
        else: return self.iterateGroups( list )[ list[-1] ]

####### Get Variables
    def variables( self ):

        vars = []
        for weight in self.weights: vars.append( weight[0] )
        for bias   in self.biases:  vars.append( bias[0]   )

        return vars

####### Get Eval
    def eval( self , names , dict = None ):

        tensors = self.tensor_list( names )
        if len( tensors ) == 1 : tensors = tensors[0]

        return self.sess.run( tensors , feed_dict = dict )

####### Get Run
    def run( self , names , inputs , use_dropout = True ):

        dict = {}

        for data in inputs:
            dict[ self.tensor( data[0] ) ] = data[1]

        for i in range( len( self.dropouts ) ):
            if use_dropout: dict[ self.dropouts[i][0] ] = self.dropouts[i][1]
            else:           dict[ self.dropouts[i][0] ] = 1.0

        return self.eval( names , dict )

####### Get Shape
    def shape( self , name ):

        return tf_shape( self.tensor( name ) )

####### Print Nodes
    def printNodes( self ):

        print( '######################################################################## TENSORS' )

        print( '#################################################### INPUTS' )
        for input in self.inputs:

            name = input[1]
            print( '********************************* ' + name )
            print( 'S:' , tf_shape( self.tensor( name ) ) )

        print( '#################################################### LAYERS' )
        for layer in self.layers:

            input , name = layer[1]['input'] , layer[1]['layer_name']
            print( '********************************* ' + input + ' --> ' + name )
            print( 'I:' , tf_shape( self.tensor( layer[1]['input'] ) ) )
            print( 'O:' , tf_shape( layer[0] ) )
            print( 'W:' , tf_shape( self.tensor( 'W_' + name ) ) )
            print( 'b:' , tf_shape( self.tensor( 'b_' + name ) ) )

        print( '#################################################### OPERATIONS' )
        for operation in self.operations:

            inputs , name = operation[1]['input'] , operation[1]['name']
            print( '********************************* ' + name )
            if not isinstance( inputs , list ): print( inputs )
            else: [ print( 'I:' , input ) for input in inputs ]

        print( '######################################################################## END TENSORS' )

####### Print Variables
    def printVariables( self ):

        print( '######################################################################## VARIABLES' )

        vars = tf.trainable_variables()
        for var in vars:
            print( var.name )

        print( '######################################################################## END VARIABLES' )

####### Create Layer Names
    def createLayerNames( self , pars ):

        name = pars['layer_type'].name()
        suf = '_' + str( self.cnt )

        if not pars['layer_name' ]: pars['layer_name' ] = name + suf
        if not pars['weight_name']: pars['weight_name'] = 'W_' + pars['layer_name']
        if not pars['bias_name'  ]: pars['bias_name'  ] = 'b_' + pars['layer_name']

        self.inserLayerNames( pars )

        pars['weight_scope'] = ''#pars['layer_name'] + '/' + pars['weight_name'] + '/'
        pars['bias_scope'  ] = ''#pars['layer_name'] + '/' + pars['bias_name'  ] + '/'
        pars['layer_scope' ] = ''#pars['layer_name'] + '/'

        return pars

####### Insert Layer Names
    def inserLayerNames( self , pars ):

        self.names[ pars['weight_name'] ] = ( 'weight' , self.cnt )
        self.names[ pars['bias_name'  ] ] = ( 'bias'   , self.cnt )
        self.names[ pars['layer_name' ] ] = ( 'layer'  , self.cnt )

####### Check for Max Pooling
    def checkMaxPooling( self , pars ):

        if np.prod( pars['max_pooling'] ) > 1:

            scope = pars['layer_scope']
            with tf.variable_scope( '' ):#self.name + scope ):

                dims = pars['layer_type'].dims()

                if dims == 2: self.layers[-1][0] = tf_maxpool2d( self.layers[-1][0] , pars )
                if dims == 3: self.layers[-1][0] = tf_maxpool3d( self.layers[-1][0] , pars )

####### Check for Activation Function
    def checkActivation( self , pars ):

        if pars['activation']:

            scope = pars['layer_scope']
            with tf.variable_scope( '' ):#self.name + scope ):
                self.layers[-1][0] = pars['activation']( self.layers[-1][0] )

####### Check for Dropout
    def checkDropout( self , pars ):

        if pars['dropout'] > 0.0:

            scope = pars['layer_name'] + '/Dropout/'
            with tf.variable_scope( '' ):#self.name + scope ):
                self.dropouts.append( [ tf_placeholder() , pars['dropout'] ] )
                self.layers[-1][0] = tf_dropout( self.layers[-1][0] , self.dropouts[-1][0] )

###### Repeat Recipe
    def repeat( self , src = None , dst = None , type = None ):

        list_src = src.split( '/' )
        src = self.iterateGroups( list_src )
        src = src[ list_src[-1] ]

        list_dst = dst.split( '/' )
        dst = self.iterateGroups( list_dst )
        pref = '/' if list_dst[0] is '' else ''
        dst.addGroup( pref + list_dst[-1] )
        dst = dst[ list_dst[-1] ]

        for tensor , name in src.inputs:
            dst.addInput( tf_shape( tensor ) , name )

        for tensor , pars in src.layers:
            if type is not None:
                pars['weight_'+ type] = src.tensor( pars['weight_name'] )
                pars['bias_'  + type] = src.tensor( pars['bias_name'  ] )
            dst.addLayer( **pars )

        for tensor , pars in src.operations:
            dst.addOperation( **pars )

        return dst

###### Copy Recipe
    def copy( self , src = None , dst = None ):

        return self.repeat( type = 'copy' , src = src , dst = dst )

###### Share Recipe
    def share( self , src = None , dst = None ):

        return self.repeat( type = 'share' , src = src , dst = dst )

####### Initialize
    def initialize( self , logdir = 'logs' , savedir = 'models' ):

        self.sess.run( tf.initialize_all_variables() )

        self.writer = tf.train.SummaryWriter( logdir , graph = self.sess.graph )
        self.summary = tf.merge_all_summaries()

        self.saver = tf.train.Saver()
        self.checkpoint = tf.train.get_checkpoint_state( savedir )
        self.checkpoint_path = savedir

####### Save
    def save( self , pref , index = None ):

        if not os.path.exists( self.checkpoint_path ):
            os.mkdir( self.checkpoint_path )

        path = self.checkpoint_path + '/' + pref
        if index is None: self.saver.save( self.sess , path )
        else: self.saver.save( self.sess , path , global_step = index )

####### Restore
    def restore( self , pref = None , index = None ):

        if pref is None:
            pref = self.checkpoint.model_checkpoint_path
        else:
            pref = self.checkpoint_path + '/' + pref
            if index is not None: pref = pref + '-' + str( index )

        self.saver.restore( self.sess , pref )

####### Train
    def train( self , train_data = None , train_labels = None , test_data = None , test_labels = None ,
               optimizer = None , eval_function = None , freq_eval = 1 ,
               size_batch = 100 , num_epochs = 10 ):

        num_samples = train_data.shape[0]
        num_batches = int( num_samples / size_batch ) + 1

        flag_eval = test_data is not None and \
                    test_labels is not None and \
                    eval_function is not None

        print( '######################################################################## TRAINING' )

        for epoch in range( num_epochs ):

            for batch in range( num_batches ):

                batch_data = get_batch( train_data , size_batch , batch )
                batch_labels = get_batch( train_labels , size_batch , batch )

                self.run( optimizer , [ [ 'Input' , batch_data   ] ,
                                        [ 'Label' , batch_labels ] ] , use_dropout = True )

            if flag_eval and (epoch + 1 ) % freq_eval == 0:

                eval = self.run( eval_function , [ [ 'Input' , test_data   ] ,
                                                   [ 'Label' , test_labels ] ] , use_dropout = False )

                print( '*** Epoch' , epoch + 1 , '| ' , end = '' )
                for i , function in enumerate( eval_function ):
                    print( function + ' :' , eval[i] , '| ' , end = '' )
                print()

        print( '######################################################################## END TRAINING' )
