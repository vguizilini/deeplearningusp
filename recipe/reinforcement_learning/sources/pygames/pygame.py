
import numpy

import pygame
import pygame.surfarray
import pygame.key

from pygame.constants import K_DOWN , K_UP , KEYDOWN , KEYUP , QUIT

### Function Intercept
def function_intercept( intercepted_func , intercepting_func ):

    def wrap( *args , **kwargs ):

        real_results = intercepted_func( *args , **kwargs )
        intercepted_results = intercepting_func( real_results , *args , **kwargs )

        return intercepted_results

    return wrap

##### PyGame
class PyGame( object ):

    ### Initialize
    def __init__( self , force_game_fps = 10 ,
                         run_real_time = False , pass_quit_event = True ):

        self.force_game_fps  = force_game_fps
        self.run_real_time   = run_real_time
        self.pass_quit_event = pass_quit_event

        self._keys_pressed = []
        self._last_keys_pressed = []

        self._playing = False

        self._default_flip       = pygame.display.flip
        self._default_update     = pygame.display.update
        self._default_event_get  = pygame.event.get
        self._default_time_clock = pygame.time.Clock
        self._default_get_ticks  = pygame.time.get_ticks

        self._game_time = 0.0

    ### Start
    def start( self ):

        if self._playing:
            raise Exception( "Already playing" )

        pygame.display.flip   = function_intercept( pygame.display.flip   , self._on_screen_update )
        pygame.display.update = function_intercept( pygame.display.update , self._on_screen_update )
        pygame.event.get      = function_intercept( pygame.event.get      , self._on_event_get     )
        pygame.time.Clock     = function_intercept( pygame.time.Clock     , self._on_time_clock    )
        pygame.time.get_ticks = function_intercept( pygame.time.get_ticks , self.get_game_time_ms  )

        self._playing = True

    ### Stop
    def stop( self ):

        if not self._playing:
            raise Exception( "Already stopped" )

        pygame.display.flip   = self._default_flip
        pygame.display.update = self._default_update
        pygame.event.get      = self._default_event_get
        pygame.time.Clock     = self._default_time_clock
        pygame.time.get_ticks = self._default_get_ticks

        self._playing = False

    ### Is Playing
    @property
    def playing( self ):
        return self._playing

    ### Set Playing
    @playing.setter
    def playing( self , value ):

        if self._playing == value:
            return

        if self._playing:
            self.stop()
        else:
            self.start()

    ### Miliseconds per Frame
    def get_ms_per_frame( self ):
        return 1000.0 / self.force_game_fps

    ### Miliseconds Game Time
    def get_game_time_ms( self ):
        return self._game_time

    ### On Time Clock
    def _on_time_clock( self , real_clock , *args , **kwargs ):
        return self._FixedFPSClock( self , real_clock )

    ### On Screen Update
    def _on_screen_update( self , _ , *args , **kwargs ):

        surface_array     = pygame.surfarray.array3d( pygame.display.get_surface() )
        reward , terminal = self.get_feedback()
        keys              = self.get_keys_pressed( surface_array , reward , terminal )

        self._last_keys_pressed = self._keys_pressed
        self._keys_pressed      = keys

        self._game_time += self.get_ms_per_frame()

    ### On Event Get
    def _on_event_get( self , _ , *args , **kwargs ):

        key_up_events = []
        if len( self._last_keys_pressed ) > 0:
            diff_list = list( set( self._last_keys_pressed ) - set( self._keys_pressed ) )
            key_up_events = [ pygame.event.Event( KEYUP , { "key" : x } ) for x in diff_list ]

        key_down_events = [ pygame.event.Event( KEYDOWN , { "key" : x } ) for x in self._keys_pressed ]

        result = []

        if args:
            if hasattr( args[0] , "__iter__" ):
                args = args[0]

            for type_filter in args:
                if type_filter == QUIT:
                    if type_filter == QUIT:
                        if self.pass_quit_event:
                            for e in _ :
                                if e.type == QUIT:
                                    result.append( e )
                    else:
                        pass
                elif type_filter == KEYUP:
                    result = result + key_up_events
                elif type_filter == KEYDOWN:
                    result = result + key_down_events
        else:
            result = key_down_events + key_up_events
            if self.pass_quit_event:
                for e in _:
                    if e.type == QUIT:
                        result.append( e )

        return result

    ### __Enter__
    def __enter__( self ):
        self.start()
        return self

    ### __Exit__
    def __exit__( self , exc_type , exc_val , exc_tb ):
        self.stop()

    ##### Fixed FPS Clock
    class _FixedFPSClock( object ):

        ### __Init__
        def __init__( self , pygame_player , real_clock ):
            self._pygame_player = pygame_player
            self._real_clock = real_clock

        ### Tick
        def tick( self , _ = None ):
            if self._pygame_player.run_real_time:
                return self._real_clock.tick( self._pygame_player.force_game_fps )
            else:
                return self._pygame_player.get_ms_per_frame()

        ### Tick Busy Loop
        def tick_busy_loop( self , _ = None ):
            if self._pygame_player.run_real_time:
                return self._real_clock.tick_busy_loop( self._pygame_player.force_game_fps )
            else:
                return self._pygame_player.get_ms_per_frame()

        ### Get Time
        def get_time( self ):
            return self._pygame_player.get_game_time_ms()

        ### Get Raw Time
        def get_raw_time( self ):
            return self._pygame_player.get_game_time_ms()

        ### Get FPS
        def get_fps( self ):
            return int( 1.0 / self._pygame_player.get_ms_per_frame() )
