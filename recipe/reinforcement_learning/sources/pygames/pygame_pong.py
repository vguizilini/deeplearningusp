
import numpy
import pygame
import random

import pygame.surfarray as surfarray

from pygame.locals import *
from sys import exit

bar_dims = ( 10 , 50 )
screen_dims = ( 640 , 480 )

black = (  0  ,  0  ,  0  )
white = ( 255 , 255 , 255 )

ball_diam = 15
ball_rad = int( ball_diam / 2 )

pygame.init()

screen = pygame.display.set_mode( screen_dims , 0 , 32 )

background_surf = pygame.Surface( screen_dims )
background = background_surf.convert() ; background.fill( black )

bar_surf = pygame.Surface( bar_dims )
bar1 = bar_surf.convert() ; bar1.fill( white )
bar2 = bar_surf.convert() ; bar2.fill( white )

ball_surf = pygame.Surface( ( ball_diam , ball_diam ) )
ball = pygame.draw.circle( ball_surf , white , ( ball_rad , ball_rad ) , ball_rad )
circle = ball_surf.convert() ; circle.set_colorkey( black )

bar1_x , bar2_x =  10.0 , 620.0
bar1_y , bar2_y = 215.0 , 215.0

circle_x , circle_y = 307.5 , 232.5
speed_x , speed_y , speed_c = 250.0 , 250.0 , 250.0

bar1_move , bar2_move = 0.0 , 0.0
bar1_score , bar2_score = 0 , 0

clock = pygame.time.Clock()
font = pygame.font.SysFont( "calibri" , 40 )

done = False
while done == False:

    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            done = True

        if event.type == KEYDOWN:
            if   event.key == K_UP   : bar1_move = - ai_speed
            elif event.key == K_DOWN : bar1_move = + ai_speed
        elif event.type == KEYUP:
            if   event.key == K_UP   : bar1_move = 0.0
            elif event.key == K_DOWN : bar1_move = 0.0
            
    score1 = font.render( str( bar1_score ) , True , white )
    score2 = font.render( str( bar2_score ) , True , white )

    screen.blit( background , ( 0 , 0 ) )

    frame_rect = pygame.draw.rect( screen , white , Rect( ( 5 , 5 ) , ( 630 , 470 ) ) , 2 )
    middle_line = pygame.draw.aaline( screen , white , ( 330 , 5 ) , ( 330 , 475 ) )

    screen.blit( bar1 , ( bar1_x , bar1_y ) )
    screen.blit( bar2 , ( bar2_x , bar2_y ) )

    screen.blit( circle , ( circle_x , circle_y ) )

    screen.blit( score1 , ( 250.0 , 210.0 ) )
    screen.blit( score2 , ( 380.0 , 210.0 ) )

    bar1_y += bar1_move
        
    time_passed = clock.tick( 30 )
    time_sec = time_passed / 1000.0
        
    circle_x += speed_x * time_sec
    circle_y += speed_y * time_sec
    ai_speed  = speed_c * time_sec
    
    if circle_x >= 305.0:

        if not bar2_y == circle_y + 7.5:

            if bar2_y < circle_y +  7.5 : bar2_y += ai_speed
            if bar2_y > circle_y - 42.5 : bar2_y -= ai_speed

        else:

            bar2_y == circle_y + 7.5
    
    if   bar1_y >= 420.0 : bar1_y = 420.0
    elif bar1_y <=  10.0 : bar1_y =  10.0

    if   bar2_y >= 420.0 : bar2_y = 420.0
    elif bar2_y <=  10.0 : bar2_y =  10.0

    if circle_x <= bar1_x + 10.0:

        if circle_y >= bar1_y - 7.5 and circle_y <= bar1_y + 42.5:
            circle_x =  20.0 ; speed_x = - speed_x

    if circle_x >= bar2_x - 15.0:

        if circle_y >= bar2_y - 7.5 and circle_y <= bar2_y + 42.5:
            circle_x = 605.0 ; speed_x = - speed_x

    if circle_x < 5.0:

        bar2_score += 1
        circle_x , circle_y = 320.0 , 232.5
        bar1_y   , bar_2_y  = 215.0 , 215.0

    elif circle_x > 620.0:

        bar1_score += 1
        circle_x , circle_y = 307.5 , 232.5
        bar1_y   , bar2_y   = 215.0 , 215.0

    if circle_y <= 10.0:
        speed_y = - speed_y ; circle_y =  10.0
    elif circle_y >= 457.5:
        speed_y = - speed_y ; circle_y = 457.5

    pygame.display.update()
            
pygame.quit()

