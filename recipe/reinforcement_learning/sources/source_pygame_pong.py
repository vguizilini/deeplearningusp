
import cv2
import numpy as np

from source import Source

from pygames.pygame import PyGame
from pygame.constants import K_DOWN , K_UP
from collections import deque

##### RosSource
class SourcePyGamePong( Source , PyGame ):

    ### __Init__
    def __init__( self , checkpoint_path = "deep_q_pong_networks" ,
                         playback_mode = False , verbose_logging = False ):

        Source.__init__( self )
        PyGame.__init__( self , force_game_fps = 8 , run_real_time = playback_mode )

        self.last_bar1_score = 0.0
        self.last_bar2_score = 0.0

    ### Get Feedback
    def get_feedback( self ):

        from pygames.pygame_pong import bar1_score , bar2_score

        score_change = ( bar1_score - self.last_bar1_score ) - \
                       ( bar2_score - self.last_bar2_score )

        self.last_bar1_score = bar1_score
        self.last_bar2_score = bar2_score

        return float( score_change ) , score_change != 0

    ### Get Params
    def get_params( self ):

        width , height , depth , scale = 80 , 80 , 1 , 1
        width = int( width / scale ) ; height = int( height / scale )

        self.dims = ( width , height )
        self.size = width * height * depth

        return [ width , height , depth ]

    ### Process Data
    def process( self , data ):

        data = cv2.cvtColor( cv2.resize( data , self.dims ) , cv2.COLOR_BGR2GRAY )
        _ , data = cv2.threshold( data , 1 , 255 , cv2.THRESH_BINARY )
        data = np.reshape( data , ( self.size ) )

        return data

    ### Key From Action
    def get_key( self ):

        if self.action[0] == 1: return [ K_DOWN ]
        if self.action[1] == 1: return []
        if self.action[2] == 1: return [ K_UP ]

    ### Start
    def start( self ):

        PyGame.start( self )
        import pygames.pygame_pong


## __Main__
if __name__ == '__main__':

    # Create and Run
    source = SourcePyGamePong()
    source.start()

    # Finish
    source.pub_params.publish( [] )
