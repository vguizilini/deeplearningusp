
import cv2
import numpy as np

import rospy
from custom_msgs.msg import Observation , Action , Parameters
from collections import deque

##### Source
class Source:

    ### __Init__
    def __init__( self ):

        # Set Variables

        self.cnt = 0
        self.fps = deque()

        # Initialize ROS

        self.init_ros()

    ### Initialize ROS
    def init_ros( self ):

        # Initialize Node
        rospy.init_node( 'Source' , anonymous = True )

        #  Publishers
        self.pub_observation = rospy.Publisher( '/dql/observation' , Observation , queue_size = 1 , tcp_nodelay = True )
        self.pub_params      = rospy.Publisher( '/dql/parameters'  , Parameters  , queue_size = 1 , tcp_nodelay = True )

        # Subscribers
        self.action , self.received_action = None , False
        rospy.Subscriber( '/dql/actions' , Action , self.callback_actions , queue_size = 1 , tcp_nodelay = True )
        self.rate = rospy.Rate( 2000 )

        # Publish Parameters
        rospy.sleep( 1 )
        self.pub_params.publish( self.get_params() )
        rospy.sleep( 1 )

    ### Callback Actions
    def callback_actions( self , msg ):

        self.action = msg.data
        self.received_action = True

    ### Publish
    def publish( self , data , feedback ):

        # Prepare Observation
        observation = Observation()
        observation.data = self.process( data )
        observation.feedback = feedback

        # Publish Observation
        self.pub_observation.publish( observation )
        self.cnt = self.cnt + 1

    ### Get Keys Pressed
    def get_keys_pressed( self , screen_array , reward , terminal ):

        # Publish Observation
        t1 = rospy.Time().now()
        self.publish( screen_array , [ reward , terminal ] )

        # Wait for Actions
        while not self.received_action : self.rate.sleep()
        self.received_action = False

        # Append FPS
        self.fps.append( ( rospy.Time().now() - t1 ).to_sec() )
        if len( self.fps ) > 100 : self.fps.popleft()

        # Return Action
        print( '*** Counter:' , self.cnt , '| Processing time:' , int( 1.0 / np.mean( self.fps ) ) , 'fps' )
        return self.get_key()
