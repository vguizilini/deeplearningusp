
import random
from player import Player
import rospy

import sys
sys.path.append( '../..' )
from Recipe import *

NUM_FRAMES  = 4
NUM_ACTIONS = 3

BATCH_SIZE = 0#100

LEARNING_RATE = 1e-6
REWARD_DISCOUNT = 0.99

EXPERIENCES_LEN = 100000
REWARD_SCORE_LEN = 200

SAVE_EVERY_X_STEPS = 10000

START_RANDOM_PROB = 0.00#1.00
FINAL_RANDOM_PROB = 0.00#0.05
NUM_EXPLORATION_STEPS = 500000
STEPS_BEFORE_TRAIN = 1000

##### RosPlayerPong
class PlayerPyGamePong( Player ):

    ### Set Variables

    num_actions = NUM_ACTIONS
    random_prob = START_RANDOM_PROB

    ### Calculate Current Action
    def calc_action( self ):

        # Read Feedback
        reward = self.feedback[0]
        terminal = self.feedback[1]

        # Store Reward Score
        if reward != 0.0:
            self.reward_score.append( reward )
            if len( self.reward_score ) > REWARD_SCORE_LEN:
                self.reward_score.popleft()

        # Create Current State
        if self.last_state is None:
            self.last_state = np.stack( tuple(
                self.observation for _ in range ( NUM_FRAMES ) ) , axis = 2 )
            self.last_action = self.create_action( 1 )
            return self.last_action

        # Update Current State
        frame = np.reshape( self.observation, ( self.observation.shape[0] ,
                                                self.observation.shape[1] , 1 ) )
        current_state = np.append( self.last_state[ : , : , 1: ] , frame , axis = 2 )

        # Store New Experience
        self.experiences.append( ( self.last_state , self.last_action , reward , current_state , terminal ) )
        if len( self.experiences ) > EXPERIENCES_LEN: self.experiences.popleft()

        # Check for Train
        if len( self.experiences ) > STEPS_BEFORE_TRAIN:
            self.train() ; self.training_steps += 1

        # Check for Random or Calculated Action
        if random.random() < self.random_prob:
            current_action = self.create_rand_action()
        else:
            current_action = self.create_calc_action( current_state )

        # Update Random Probability
        if self.random_prob > FINAL_RANDOM_PROB and len( self.experiences ) > STEPS_BEFORE_TRAIN:
            self.random_prob -= ( START_RANDOM_PROB - FINAL_RANDOM_PROB ) / NUM_EXPLORATION_STEPS

        # Update Last Information
        self.last_state = current_state
        self.last_action = current_action

        # Print
        print( '*** Counter:' , self.cnt ,
               '| Prob_Random:' , self.random_prob ,
               '| Accuracy:' , sum( self.reward_score ) / float( REWARD_SCORE_LEN ) )

        # Sheck for Save
        if self.cnt % SAVE_EVERY_X_STEPS == 0:
            print( 'SAVING' ) ; self.recipe.save( 'trained' , self.cnt )

        # Return Current Action
        return current_action

    ### Train
    def train( self ):

        # Return if Not Training
        if BATCH_SIZE == 0 : return

        # Select Batch
        batch = random.sample( self.experiences , BATCH_SIZE )

        # Separate Batch Data
        previous_states = [ d[0] for d in batch ]
        actions         = [ d[1] for d in batch ]
        rewards         = [ d[2] for d in batch ]
        current_states  = [ d[3] for d in batch ]
        is_terminal     = [ d[4] for d in batch ]

        # Calculate Rewards for each Action
        rewards_per_action = self.recipe.run( 'Output' , [ [ 'Observation' , current_states ] ] )

        # Calculate Expected Reward
        expected_reward = []
        for i in range( BATCH_SIZE ):
            if is_terminal[i]: expected_reward.append( rewards[i] )
            else:              expected_reward.append( rewards[i] + REWARD_DISCOUNT * np.max( rewards_per_action[i] ) )

        # Optimize Neural Network
        self.recipe.run( 'Optimizer' , [ [ 'Observation' , previous_states ] ,
                                         [ 'Actions'     , actions         ] ,
                                         [ 'Target'      , expected_reward ] ] )

    ### Create Network
    def create_network( self ):

        recipe = Recipe()
        recipe.addInput( [ None , self.dims[0] , self.dims[1] , NUM_FRAMES ] , name = 'Observation' )

        recipe.setLayerDefaults( layer_type = tf_layer_conv2d ,
                                 activation = tf_relu , max_pooling = 2 )
        recipe.addLayer( out_channels = 32 , strides = 4 , ksize = 8 , input = 'Observation' )
        recipe.addLayer( out_channels = 64 , strides = 2 , ksize = 4 )
        recipe.addLayer( out_channels = 64 , strides = 1 , ksize = 3 )

        recipe.setLayerDefaults( layer_type = tf_layer_fully )
        recipe.addLayer( out_channels = 256 )
        recipe.addLayer( out_channels = NUM_ACTIONS , activation = None , layer_name = 'Output' )

        return recipe

    ### Create Learning
    def create_learning( self , recipe ):

        recipe.addInput( [ None , NUM_ACTIONS ] , name = 'Actions' )
        recipe.addInput( [ None               ] , name = 'Target'  )

        recipe.addOperation( function = tf_sum_mul          , input = [ 'Output' , 'Actions' ] , name = 'Readout' )
        recipe.addOperation( function = tf_mean_square_diff , input = [ 'Target' , 'Readout' ] , name = 'Cost' )

        recipe.addOperation( function = tf_adam_optimizer   , input = 'Cost' ,
                             learning_rate = LEARNING_RATE , name = 'Optimizer' )

        recipe.initialize( savedir = 'models_pygame_pong' )
#        recipe.restore()

        return recipe

## __Main__
if __name__ == '__main__':

    player = PlayerPyGamePong()
    player.start()
