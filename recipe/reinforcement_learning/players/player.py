
import random
import numpy as np

import rospy
from custom_msgs.msg import Observation , Action , Parameters
from collections import deque

##### Player
class Player():

    ### Set Variables

    training_steps = 0
    last_state , last_action = None , None

    experiences = deque()
    reward_score = deque()

    ### Create Action
    def create_action( self , index ):

        action = np.zeros( self.num_actions )
        action[ index ] = 1

        return action

    ### Create Calculated Action
    def create_calc_action( self , current_state ):

        output = self.recipe.run( 'Output' , [ [ 'Observation' , [ current_state ] ] ] )[0]
        return self.create_action( np.argmax( output ) )

    ### Create Random Action
    def create_rand_action( self ):

        return self.create_action( random.randint( 0 , self.num_actions - 1 ) )

    ### Initialise ROS
    def init_ros( self ):

        # Initialize Variables

        self.cnt = 0
        self.running = False

        # Initialize Node
        rospy.init_node( 'Player' , anonymous = True )

        # Subscribers
        rospy.Subscriber( '/dql/observation' , Observation , self.callback_observation , queue_size = 1 , tcp_nodelay = True )
        rospy.Subscriber( '/dql/parameters'  , Parameters  , self.callback_parameters  , queue_size = 1 , tcp_nodelay = True )

        # Publishers
        self.pub_actions = rospy.Publisher( '/dql/actions' , Action , queue_size = 1 , tcp_nodelay = True )

        # Wait for Source Connection
        print( 'WAITING FOR SOURCE ...' )
        while not self.running: rospy.sleep( 1 )
        print( 'RECEIVED!' )

        # Spin ROS
        rospy.spin()

    ### Callback Observation
    def callback_observation( self , msg ):

        # Store Observation
        self.observation = np.reshape( msg.data , self.dims )
        self.feedback = msg.feedback

        # Calculate Action
        action = self.calc_action()
        self.pub_actions.publish( action )

        # Update Counter
        self.cnt += 1

    ### Callback Parameters
    def callback_parameters( self , msg ):

        # Switch Running Flag
        self.running = not self.running

        # If Started
        if self.running:

            # Store Parameters

            width , height , depth = msg.data
            if depth == 1 : self.dims = ( int( width ) , int( height ) )
            else:           self.dims = ( int( width ) , int( height ) , int( depth ) )

            # Prepare Learning

            self.recipe = self.create_network()
            self.recipe = self.create_learning( self.recipe )

        # Shutdown if Finished
        else: rospy.signal_shutdown( 'ENDING' )


    ### Start
    def start( self ):

        # Initialize ROS nodes
        self.init_ros()
