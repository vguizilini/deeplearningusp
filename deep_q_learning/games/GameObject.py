
from collections import defaultdict
from IPython.display import clear_output , display

from utils import svg
from euclid import Circle , Point2 , Vector2 , LineSegment2

########################################################################################################
### Game Object
########################################################################################################

class GameObject( object ):

    ### Initialize
    def __init__( self , position , speed , obj_type , settings ):

        self.settings = settings
        self.radius = self.settings[ "object_radius" ]

        self.obj_type = obj_type
        self.position = position
        self.speed    = speed

        self.bounciness = 1.0

    ### Wall Collisions
    def wall_collisions( self ):

        world_size = self.settings[ "world_size" ]

        for dim in range(2):
            if   self.position[dim] - self.radius     <=        0        and self.speed[dim] < 0:
                self.speed[dim] = - self.speed[dim] * self.bounciness
            elif self.position[dim] + self.radius + 1 >= world_size[dim] and self.speed[dim] > 0:
                self.speed[dim] = - self.speed[dim] * self.bounciness

    ### Move
    def move( self , dt ):

        self.position += dt * self.speed
        self.position = Point2( *self.position )

    ### Step
    def step( self , dt ):

        self.wall_collisions()
        self.move( dt )

    ### As Circle
    def as_circle( self ):

        return Circle( self.position , float( self.radius ) )

    ### Draw
    def draw( self ):

        color = self.settings[ "colors" ][ self.obj_type ]
        return svg.Circle( self.position + Point2( 10 , 10 ) ,
                           self.radius , color = color )

