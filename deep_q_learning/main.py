
import numpy as np
import tensorflow as tf

from controllers.DeepController import *
from games.KarpathyGame import *
from simulations.Simulation import *
from models.MultiLayer import *

settings = {

    'objects': [
        'friend' ,
        'enemy'  ,
    ] ,

    'colors': {
        'friend' : 'green'  ,
        'enemy'  : 'red'    ,
        'hero'   : 'yellow' ,
    } ,

    'object_reward': {
        'friend' : - 1 ,
        'enemy'  : + 1 ,
    } ,

    "num_objects": {
        "friend" : 50 ,
        "enemy"  : 25 ,
    } ,

    'hero_bounces_off_walls' : False ,

    'world_size'            : ( 700 , 500 ) ,
    'hero_initial_position' : [ 400 , 300 ] ,
    'hero_initial_speed'    : [   0 ,   0 ] ,
    "maximum_speed"         : [  50 ,  50 ] ,

    "object_radius"           :  10.0 ,
    "observation_line_length" : 120.0 ,
    "wall_distance_penalty"   :   0.0 ,

    "num_observation_lines"      : 32 ,
    "tolerable_distance_to_wall" : 50 ,
    "delta_v"                    : 50 ,
}

game = KarpathyGame( settings )

tf.reset_default_graph()
session = tf.InteractiveSession()

brain = MultiLayer( [ game.observation_size , ] ,
                    [ 200 , 200 , game.num_actions ] ,
                    [ tf.tanh , tf.tanh , tf.identity ] )
    
optimizer = tf.train.RMSPropOptimizer(
                        learning_rate = 0.001 , decay = 0.9 )

current_controller = DeepController( ( game.observation_size , ) , game.num_actions , brain , optimizer , session ,
                                     discount_rate = 0.99 , exploration_period = 5000 , max_experience = 10000 ,
                                     store_every_nth = 4 , train_every_nth = 4 )
    
session.run( tf.initialize_all_variables() )
session.run( current_controller.target_network_update )

FPS = 30
WAIT = False

ACTION_EVERY = 3
VISUALIZE_EVERY = 50
    
try:
    with tf.device( "/gpu:0" ):
        Simulation( game = game , controller = current_controller , fps = FPS ,
                    visualize_every = VISUALIZE_EVERY , action_every = ACTION_EVERY , wait = WAIT ,
                    training = True , simulation_resolution = 0.001 , save_path = 'figures' )
except KeyboardInterrupt:
    print("Interrupted")

