
import math
import time

from os import makedirs
from os.path import join , exists

from IPython.display import clear_output , display

from itertools import count
import matplotlib.pyplot as plt

### Simulation
def Simulation( game = None , controller = None ,
                fps = 60 , visualize_every = 1 , action_every = 1 ,
                simulation_resolution = None , wait = False , training = True ,
                save_path = None ):

    if save_path is not None:
        if not exists( save_path ):
            makedirs( save_path )
    last_image = 0

    chunks_per_frame = 1
    chunk_length_s   = 1.0 / fps

    if simulation_resolution is not None:

        frame_length_s = 1.0 / fps
        chunks_per_frame = int( math.ceil( frame_length_s / simulation_resolution ) )
        chunks_per_frame = max( chunks_per_frame , 1 )
        chunk_length_s = frame_length_s / chunks_per_frame

    last_observation = None
    last_action      = None

    simulation_started_time = time.time()

    if hasattr( game , 'setup_draw' ):
        game.setup_draw()

    for frame_no in count():

        for _ in range( chunks_per_frame ):
            game.step( chunk_length_s )

        if frame_no % action_every == 0:

            new_observation = game.observe()
            reward          = game.collect_reward()

            if last_observation is not None:
                controller.store( last_observation , last_action , reward , new_observation )

            new_action = controller.action( new_observation )
            game.perform_action( new_action )

            if training:
                controller.training_step()

            last_action = new_action
            last_observation = new_observation

        if ( frame_no + 1 ) % visualize_every == 0:

            fps_estimate = frame_no / ( time.time() - simulation_started_time )

            stats = [ "fps = %.1f" % ( fps_estimate , ) ]
            clear_output( wait = True )

            sketch = game.sketch( stats )
            display( sketch )

            if save_path is not None:

                img_path = join( save_path , "%d.svg" % ( last_image , ) )
                with open( img_path , "w" ) as f:
                    sketch.write_svg( f )
                last_image += 1

        time_should_have_passed = frame_no / fps
        time_passed = ( time.time() - simulation_started_time )
        if wait and ( time_should_have_passed > time_passed ):
            time.sleep( time_should_have_passed - time_passed )
