
import os

import tensorflow as tf

### Color String
def colorstr( rgb ):

    if type( rgb ) == tuple:
        return "#%02x%02x%02x" % rgb
    else:
        return rgb

### Compute Color
def compute_color( style ):

    color = style.get( "color" )
    if color is None: color = "none"
    return color

### Compute Fill
def compute_fill( style ):

    color = compute_color( style )

    style_str = []
    style_str.append( 'fill:%s;' % ( colorstr( color ) , ) )
    style_str = 'style="%s"' % ( ';'.join( style_str ) , )

    return style_str

### Compute Stroke
def compute_stroke( style ):

    color = compute_color( style )

    style_str = []
    style_str.append( 'stroke:%s;' % ( colorstr( color ) , ) )
    style_str = 'style="%s"' % ( ';'.join( style_str ) , )

    return style_str

### Scene Class
class Scene:

    ### Initialize
    def __init__( self , size = ( 400 , 400 ) ):

        self.items = []
        self.size = size

    ### Add
    def add( self , item ):

        self.items.append( item )

    ### String Array
    def strarray( self ):

        var = [
           "<?xml version=\"1.0\"?>\n" ,
           "<svg height=\"%d\" width=\"%d\" >\n" % ( self.size[1] , self.size[0] ) ,
           " <g style=\"fill-opacity:1.0; stroke:black;\n" ,
           "  stroke-width:1;\">\n"
        ]

        for item in self.items : var += item.strarray()
        var += [" </g>\n</svg>\n"]
        return var

    ### Write SVG
    def write_svg( self , file ):

        file.writelines( self.strarray() )

### Line Class
class Line:

    ### Initizlie
    def __init__( self , start , end , **style_kwargs ):

        self.start = start
        self.end = end
        self.style_kwargs = style_kwargs

    ### String Array
    def strarray( self ):

        style_str = compute_stroke( self.style_kwargs )
        return [ "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\"\n" %\
                ( self.start[0] , self.start[1] , self.end[0] , self.end[1] ) ,
                 "          %s />\n" % ( style_str , ) ]

### Circle Class
class Circle:

    ### Initialize
    def __init__( self , center , radius , **style_kwargs ):

        self.center = center
        self.radius = radius
        self.style_kwargs = style_kwargs

    ### String Array
    def strarray( self ):

        style_str = compute_fill( self.style_kwargs )
        return [ "  <circle cx=\"%d\" cy=\"%d\" r=\"%d\"\n" % ( self.center[0] , self.center[1] , self.radius ) ,
                 "          %s />\n" % ( style_str , ) ]

### Rectangle Class
class Rectangle:

    ### Initialize
    def __init__( self , origin , size , **style_kwargs ):

        self.origin = origin
        self.size = size
        self.style_kwargs = style_kwargs

    ### String Array
    def strarray( self ):

        style_str = compute_fill(self.style_kwargs)
        return [ "  <rect x=\"%d\" y=\"%d\" height=\"%d\"\n" % ( self.origin[0] , self.origin[1] , self.size[1] ) ,
                 "        width=\"%d\" %s />\n" % ( self.size[0] , style_str ) ]

### Text Class
class Text:

    ### Initialize
    def __init__( self , origin , text , size = 24 , color = 'black' ):

        self.origin = origin
        self.text = text
        self.size = size
        self.color = color

        return

    ### String Array
    def strarray( self ):

        return [ "  <text x=\"%d\" y=\"%d\" fill=\"%s\" stroke=\"%s\" font-size=\"%d\">\n" % \
                                            ( self.origin[0] , self.origin[1] , self.color , self.color , self.size ) ,
                 "   %s\n" % self.text,
                 "  </text>\n"]


