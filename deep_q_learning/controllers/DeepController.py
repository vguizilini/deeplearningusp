
import os
import pickle
import time
import random

import numpy as np
import tensorflow as tf

from collections import deque

########################################################################################################
### Deep Controller
########################################################################################################

class DeepController( object ):

    ### Initialize
    def __init__( self , observation_shape ,
                  num_actions , observation_to_actions , optimizer , session ,
                  random_action_probability = 0.05 , exploration_period = 1000 ,
                  store_every_nth = 5 , train_every_nth = 5 ,
                  minibatch_size = 32 , discount_rate = 0.95 , max_experience = 30000 ,
                  target_network_update_rate = 0.01 , summary_writer = None ):

        self.observation_shape         = observation_shape
        self.num_actions               = num_actions

        self.q_network                 = observation_to_actions
        self.optimizer                 = optimizer
        self.s                         = session

        self.random_action_probability = random_action_probability
        self.exploration_period        = exploration_period
        self.store_every_nth           = store_every_nth
        self.train_every_nth           = train_every_nth
        self.minibatch_size            = minibatch_size
        self.discount_rate             = tf.constant( discount_rate )
        self.max_experience            = max_experience

        self.target_network_update_rate = \
                tf.constant( target_network_update_rate )

        self.actions_executed_so_far = 0
        self.experience = deque()

        self.iteration = 0
        self.summary_writer = summary_writer

        self.number_of_times_store_called = 0
        self.number_of_times_train_called = 0

        self.create_variables()

        self.s.run(tf.initialize_all_variables())
        self.s.run( self.target_network_update )

        self.saver = tf.train.Saver()

    ### Linear Annealing
    def linear_annealing( self , n , total , p_initial , p_final ):

        if n >= total:
            return p_final
        else:
            return p_initial - ( n * ( p_initial - p_final ) ) / ( total )

    ### Observation Batch Shape
    def observation_batch_shape( self , batch_size ):

        return tuple( [ batch_size ] + list( self.observation_shape ) )

    ### Create Variables
    def create_variables( self ):

        self.target_q_network = self.q_network.copy( scope = "target_network" )

        with tf.name_scope( "taking_action" ):

            self.observation   = tf.placeholder( tf.float32 , self.observation_batch_shape( None ) , name = "observation" )
            self.action_scores = tf.identity( self.q_network( self.observation ) , name = "action_scores" )

            tf.histogram_summary( "action_scores" , self.action_scores )
            self.predicted_actions = tf.argmax( self.action_scores , dimension = 1 , name = "predicted_actions" )

        with tf.name_scope( "estimating_future_rewards" ):

            self.next_observation      = tf.placeholder( tf.float32 , self.observation_batch_shape( None ) , name = "next_observation" )
            self.next_observation_mask = tf.placeholder( tf.float32 , ( None , ) , name = "next_observation_mask" )
            self.next_action_scores    = tf.stop_gradient( self.target_q_network( self.next_observation ) )

            tf.histogram_summary( "target_action_scores" , self.next_action_scores )
            self.rewards        = tf.placeholder( tf.float32 , ( None , ) , name = "rewards" )
            target_values       = tf.reduce_max( self.next_action_scores , reduction_indices = [ 1 , ] ) * self.next_observation_mask
            self.future_rewards = self.rewards + self.discount_rate * target_values

        with tf.name_scope( "q_value_precition" ):

            self.action_mask          = tf.placeholder( tf.float32 , ( None , self.num_actions ) , name = "action_mask" )
            self.masked_action_scores = tf.reduce_sum( self.action_scores * self.action_mask , reduction_indices = [ 1 , ] )
            temp_diff                 = self.masked_action_scores - self.future_rewards
            self.prediction_error     = tf.reduce_mean( tf.square( temp_diff ) )
            gradients                 = self.optimizer.compute_gradients( self.prediction_error )

            for i , ( grad , var ) in enumerate( gradients ):
                if grad is not None:
                    gradients[i] = ( tf.clip_by_norm( grad , 5 ) , var )

            for grad , var in gradients:
                tf.histogram_summary( var.name , var )
                if grad is not None:
                    tf.histogram_summary( var.name + '/gradients' , grad )

            self.train_op = self.optimizer.apply_gradients( gradients )

        with tf.name_scope( "target_network_update" ):

            self.target_network_update = []
            for v_source , v_target in zip( self.q_network.variables() , self.target_q_network.variables() ):

                update_op = v_target.assign_sub( self.target_network_update_rate * ( v_target - v_source ) )
                self.target_network_update.append( update_op )

            self.target_network_update = tf.group( *self.target_network_update )

        tf.scalar_summary( "prediction_error" , self.prediction_error )
        self.summarize = tf.merge_all_summaries()
        self.no_op1    = tf.no_op()

    ### Action
    def action( self , observation ):

        self.actions_executed_so_far += 1
        exploration_p = self.linear_annealing( self.actions_executed_so_far , self.exploration_period ,
                                               1.0 , self.random_action_probability )

        if random.random() < exploration_p:
            return random.randint( 0 , self.num_actions - 1 )
        else:
            return self.s.run( self.predicted_actions ,
                    { self.observation : observation[ np.newaxis , : ] } )[0]

    ### Exploration Completed
    def exploration_completed( self ):

        return min( float( self.actions_executed_so_far ) / self.exploration_period , 1.0 )

    ### Store
    def store( self , observation , action , reward , newobservation ):

        if self.number_of_times_store_called % self.store_every_nth == 0:
            self.experience.append( ( observation , action , reward , newobservation ) )
            if len( self.experience ) > self.max_experience:
                self.experience.popleft()

        self.number_of_times_store_called += 1

    ### Training Step
    def training_step( self ):

        if self.number_of_times_train_called % self.train_every_nth == 0:

            if len( self.experience ) <  self.minibatch_size:
                return

            samples = random.sample( range( len( self.experience ) ) , self.minibatch_size )
            samples = [ self.experience[i] for i in samples ]

            states      = np.empty( self.observation_batch_shape( len( samples ) ) )
            newstates   = np.empty( self.observation_batch_shape( len( samples ) ) )
            action_mask = np.zeros( ( len( samples ) , self.num_actions ) )

            newstates_mask = np.empty( ( len( samples ) , ) )
            rewards        = np.empty( ( len( samples ) , ) )

            for i , ( state , action , reward , newstate ) in enumerate( samples ):

                states[i] = state
                action_mask[i] = 0
                action_mask[i][action] = 1
                rewards[i] = reward

                if newstate is not None:
                    newstates[i] = newstate
                    newstates_mask[i] = 1
                else:
                    newstates[i] = 0
                    newstates_mask[i] = 0

            calculate_summaries = self.iteration % 100 == 0 and \
                    self.summary_writer is not None

            cost , _ , summary_str = self.s.run( [ self.prediction_error , self.train_op ,
                                                   self.summarize if calculate_summaries else self.no_op1 ] , {
                                                        self.observation           : states ,
                                                        self.next_observation      : newstates ,
                                                        self.next_observation_mask : newstates_mask ,
                                                        self.action_mask           : action_mask ,
                                                        self.rewards               : rewards } )

            self.s.run( self.target_network_update )

            if calculate_summaries:
                self.summary_writer.add_summary( summary_str , self.iteration )

            self.iteration += 1

        self.number_of_times_train_called += 1

    ### Save
    def save( self , save_dir , debug = False ):

        STATE_FILE = os.path.join( save_dir , 'deepq_state' )
        MODEL_FILE = os.path.join( save_dir , 'model' )

        state = {
            'actions_executed_so_far'      : self.actions_executed_so_far ,
            'iteration'                    : self.iteration ,
            'number_of_times_store_called' : self.number_of_times_store_called ,
            'number_of_times_train_called' : self.number_of_times_train_called ,
        }

        if debug:
            print( 'Saving model... ' )

        saving_started = time.time()

        self.saver.save( self.s , MODEL_FILE )
        with open( STATE_FILE , "wb" ) as f:
            pickle.dump( state , f )

        print( 'done in {} s'.format( time.time() - saving_started ) )

    ### Restore
    def restore( self , save_dir , debug = False ):

        STATE_FILE = os.path.join( save_dir , 'deepq_state' )
        MODEL_FILE = os.path.join( save_dir , 'model' )

        with open( STATE_FILE , "rb" ) as f:
            state = pickle.load( f )
        self.saver.restore( self.s , MODEL_FILE )

        self.actions_executed_so_far      = state[ 'actions_executed_so_far' ]
        self.iteration                    = state[ 'iteration' ]
        self.number_of_times_store_called = state[ 'number_of_times_store_called' ]
        self.number_of_times_train_called = state[ 'number_of_times_train_called' ]



